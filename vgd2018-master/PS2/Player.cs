using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using XELibrary;

namespace PS2
{
    public class Player : Microsoft.Xna.Framework.DrawableGameComponent 
    {
        ICelAnimationManager celAnimationManager;
        IInputHandler inputHandler;
        public List<Food> foodList;
        SpriteBatch spriteBatch;
        public int points = 0;
        public bool gameOver = false;
        public Rectangle Rectangle
        {
            get
            {
                return new Rectangle((int)Position.X, (int)Position.Y, celAnimationManager.GetAnimationFrameWidth("kirbyRun"), celAnimationManager.GetAnimationFrameHeight("kirbyRun"));
            }
        }

        private Vector2 position = new Vector2(20, 520);
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        enum Direction { Left, Right,Up,Down };

        private Direction direction = Direction.Right;
        private float moveRate = 3.0f;

        public Player(Game game, List<Food> _foodList)
            : base(game)
        {
            celAnimationManager = (ICelAnimationManager)game.Services.GetService(typeof(ICelAnimationManager));
            inputHandler = (IInputHandler)game.Services.GetService(typeof(IInputHandler));
            foodList = _foodList;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public void Load(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
        }

        protected override void LoadContent()
        {
            CelCount kirbycelCount = new CelCount(7, 1);
         //   CelCount foodcelCount = new CelCount(7, 2);

            celAnimationManager.AddAnimation("kirbyRun", "kirby", kirbycelCount, 10);
            celAnimationManager.ToggleAnimation("kirbyRun");

        }

        public override void Update(GameTime gameTime)
        {
            if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Right))
            {
                celAnimationManager.ResumeAnimation("kirbyRun");
                direction = Direction.Right;
                position.X += moveRate;
            }
            else if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Left))
            {
                celAnimationManager.ResumeAnimation("kirbyRun");
                direction = Direction.Left;
                position.X -= moveRate;
            }
            else
                celAnimationManager.PauseAnimation("kirbyRun");

            int celWidth = celAnimationManager.GetAnimationFrameWidth("kirbyRun");
            int celHeight = celAnimationManager.GetAnimationFrameHeight("KirbyRun");

            if (position.X > (Game.GraphicsDevice.Viewport.Width - celWidth))
                position.X =  (Game.GraphicsDevice.Viewport.Width - celWidth);
            if (position.X < 0)
                position.X = 0;


            foreach(Food food in foodList)
            {
                if (Rectangle.Intersects(food.Rectangle)&& food.texture.Name != "poisenIcon")
                {
                    food.IsRemoved = true;
                    points++;
                }
                else
                    if(Rectangle.Intersects(food.Rectangle) && food.texture.Name == "poisenIcon")
                    {
                    food.IsRemoved = true;
                    gameOver = true;
                    }
            }
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            celAnimationManager.Draw(gameTime, "kirbyRun", spriteBatch, position, direction == Direction.Right ? SpriteEffects.None : SpriteEffects.FlipHorizontally);
          
            spriteBatch.End();
        }
    }
}
