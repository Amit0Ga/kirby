﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS2
{
    public class Food:Sprite    {

        public Texture2D texture;
        public Food(Texture2D _texture)
    : base(_texture)
        {
            Position = new Vector2(Game1.Random.Next(0, Game1.ScreenWidth - _texture.Width), 0);
            Speed = Game1.Random.Next(2, 5);
            texture = _texture;

        }

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            Position.Y = Position.Y+Speed;

            // If we hit the bottom of the window
            if (Rectangle.Bottom >= Game1.ScreenHeight)
                IsRemoved = true;
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_texture, Position, Color.White);
        }
    }
}
