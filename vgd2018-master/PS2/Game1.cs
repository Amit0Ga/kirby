using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using XELibrary;

namespace PS2
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        private SpriteFont pointsFont;
        private Vector2 pointsPos;
        private Vector2 anouncePos;



        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D background;
        Texture2D kiwi;
        Texture2D donut;
        Texture2D watermelon;
        Texture2D cake;
        Texture2D chips;
        Texture2D popcorn;
        Texture2D bagel;
        Texture2D poisen;

        public Food Kiwi;
        public Food Donut;
        public Food Watermelon;
        public Food Cake;
        public Food Chips;
        public Food Popcorn;
        public Food Bagel;
        public Food Poisen;

        public String[] foodName =  new String []{"kiwiIcon","chipsIcon","cakeIcon","watermelonIcon","popcornIcon","donutIcon","bagelIcon","poisenIcon"};

        public List<Food> foodList ; 
        
        

        private CelAnimationManager celAnimationManager;
        public static Random Random;

        private InputHandler inputHandler;

        private Player player;
        private List<Sprite> _sprites;

        private float _timer;
        public static int ScreenWidth = 1000;
        public static int ScreenHeight = 600;
        public Game1()
        {
           pointsPos = new Vector2(50, 50);
            anouncePos = new Vector2(ScreenWidth / 3, ScreenHeight / 2);
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = ScreenWidth;
            graphics.PreferredBackBufferHeight = ScreenHeight;

            
            Random = new Random();

            Content.RootDirectory = "Content";

            inputHandler = new InputHandler(this);
            Components.Add(inputHandler);

            celAnimationManager = new CelAnimationManager(this, "Textures\\");
            Components.Add(celAnimationManager);

            player = new Player(this, foodList);
            Components.Add(player);
        }

        protected override void Initialize()
        {

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            background = Content.Load<Texture2D>(@"Textures/background");
            kiwi = Content.Load<Texture2D>(@"Textures/kiwiIcon");
            watermelon = Content.Load<Texture2D>(@"Textures/watermelonIcon");
            chips = Content.Load<Texture2D>(@"Textures/chipsIcon");
            popcorn = Content.Load<Texture2D>(@"Textures/popcornIcon");
            cake = Content.Load<Texture2D>(@"Textures/cakeIcon");
            bagel = Content.Load<Texture2D>(@"Textures/bagelIcon");
            donut = Content.Load<Texture2D>(@"Textures/donutIcon");
            poisen = Content.Load<Texture2D>(@"Textures/poisenIcon");

            pointsFont = Content.Load<SpriteFont>(@"Fonts\Arial");


            Kiwi = new Food(kiwi);
            Chips = new Food(chips);
            Donut = new Food(donut);
            Watermelon = new Food(watermelon);
            Bagel = new Food(bagel);
            Popcorn = new Food(popcorn);
            Cake = new Food(cake);
            Poisen = new Food(poisen);

            Food[] foodIcons = new Food[8];
            foodIcons[0] = Kiwi;
            foodIcons[1] = Chips;
            foodIcons[2] = Donut;
            foodIcons[3] = Cake;
            foodIcons[4] = Watermelon;
            foodIcons[5] = Bagel;
            foodIcons[6] = Popcorn;
            foodIcons[7] = Poisen;

            foodList = new List<Food>() { Kiwi, Donut, Watermelon, Cake, Popcorn, Chips, Bagel,Poisen };
            player.foodList = this.foodList;
            _sprites = new List<Sprite>();


            player.Load(spriteBatch);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {

            if (!player.gameOver)
            {
                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                    this.Exit();
                _timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

                player.Update(gameTime);
                //  _sprites.Add(new Food(Content.Load<Texture2D>(foodName[Random.Next(0, 7)])));
                //       foodList.Add(new Food(Content.Load<Texture2D>(foodName[Random.Next(0, 7)])));



                if (_timer > 0.25f/*&&foodList.Count<=15*/)
                {
                    _timer = 0f;
                    //  _sprites.Add(new Food(Content.Load<Texture2D>(foodName[Random.Next(0, 7)])));
                    foodList.Add(new Food(Content.Load<Texture2D>(foodName[Random.Next(0, 8)])));
                }
                foreach (Food food in foodList.ToArray())
                {
                    food.Update(gameTime, _sprites);
                    if (food.IsRemoved)
                        foodList.Remove(food);
                }

                for (int i = 0; i < _sprites.Count; i++)
                {
                    var sprite = _sprites[i];

                    if (sprite.IsRemoved)
                    {
                        _sprites.RemoveAt(i);
                        i--;
                    }

                }
                base.Update(gameTime);

            }



        }

        protected override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Draw(background, new Vector2(0, 0),new Rectangle(0,0,1000,600), Color.White);
            spriteBatch.DrawString(pointsFont, player.points.ToString(), pointsPos, Color.White);

            foreach (Food food in foodList)
            {
                food.Draw(spriteBatch);
            }
            if (player.gameOver)
            {
                spriteBatch.DrawString(pointsFont, "You lose!", anouncePos, Color.White);

            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
