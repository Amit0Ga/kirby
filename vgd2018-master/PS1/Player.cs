using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary; // for InputHandler

namespace PS1
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Player : Microsoft.Xna.Framework.GameComponent
    {
        private Vector2 position;
        public Vector2 Position
        {
            get { return position; }
        }

        private readonly float moveRate;

        private IInputHandler input;
        private SpriteBatch spriteBatch;

        public Player(Game game)
            : base(game)
        {
            input = (IInputHandler)game.Services.GetService(typeof(IInputHandler));

            position = new Vector2(1000.0f, 400.0f);
            moveRate = 200.0f;
        }

        public void Load(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            if (input.KeyboardHandler.IsKeyDown(Keys.Right))
                position.X += moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (input.KeyboardHandler.IsKeyDown(Keys.Left))
                position.X -= moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (input.KeyboardHandler.IsKeyDown(Keys.Up))
                position.Y -= moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (input.KeyboardHandler.IsKeyDown(Keys.Down))
                position.Y += moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (position.X > Game.GraphicsDevice.Viewport.Width - 50)
                position.X = Game.GraphicsDevice.Viewport.Width - 50;
            if (position.X < 0)
                position.X = 0;
            if (position.Y > Game.GraphicsDevice.Viewport.Height - 50)
                position.Y = Game.GraphicsDevice.Viewport.Height - 50;
            if (position.Y < 0)
                position.Y = 0;

            
            base.Update(gameTime);
        }
    }
}
