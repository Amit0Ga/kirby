using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace PS1
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Enemy : Microsoft.Xna.Framework.GameComponent
    {
        private Vector2 position;
        public Vector2 Position
        {
            get { return position; }
        }

        private readonly float moveRate;

        private SpriteBatch spriteBatch;

        // Acts according to player position and behaviour
        private Player player;

        public Enemy(Game game, Player player)
            : base(game)
        {
            position = new Vector2(500.0f, 200.0f);
            moveRate = 150.0f;

            this.player = player;
        }

        public void Load(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // If the player is far enough, do nothing.
            if (Vector2.Distance(position, player.Position) <= 300)
            {
                // Attempt to run away from the player at 80% chance
                Random runAway = new Random();
                if (runAway.NextDouble() >= 0.2)
                {
                    if (position.X > player.Position.X)
                        position.X += moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    else if (position.X < player.Position.X)
                        position.X -= moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;

                    if (position.Y > player.Position.Y)
                        position.Y += moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    else if (position.Y < player.Position.Y)
                        position.Y -= moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                }
                // Run in a random direction
                else
                {
                    Random rand = new Random();
                    switch (rand.Next(4))
                    {
                        case 0:
                            position.X += moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                            break;
                        case 1:
                            position.X -= moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                            break;
                        case 2:
                            position.Y += moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                            break;
                        case 3:
                            position.Y -= moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
                            break;
                    }
                }
            }

            if (position.X > Game.GraphicsDevice.Viewport.Width - 50)
                position.X = Game.GraphicsDevice.Viewport.Width - 50;
            if (position.X < 0)
                position.X = 0;
            if (position.Y > Game.GraphicsDevice.Viewport.Height - 50)
                position.Y = Game.GraphicsDevice.Viewport.Height - 50;
            if (position.Y < 0)
                position.Y = 0;

            base.Update(gameTime);
        }
    }
}
